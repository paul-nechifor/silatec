// @flow strict

import React from "react";
import FontSettings from "./FontSettings";
import Syllable from "./Syllable";

export default function SyllableRepr({
  syllable,
  settings
}: {
  syllable: Syllable,
  settings: FontSettings
}) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width={settings.charSize}
      height={settings.charSize}
      viewBox={`0 0 ${settings.charSize} ${settings.charSize}`}
      style={{ display: "inline-block" }}
    >
      <path
        d={syllable.getSvgPath(settings)}
        stroke="#000"
        fill="transparent"
        strokeWidth={`${settings.visibleStroke}px`}
      />
    </svg>
  );
}
