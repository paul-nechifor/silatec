// @flow strict

import CharPartition from "./CharPartition";
import CharPath from "./CharPath";
import Point from "./Point";
import Syllable from "./Syllable";
import SyllablePart from "./SyllablePart";
import type { MoveTo, LineTo } from "./Point";

type PointTypeData = [LineTo, number, number] | [MoveTo, number, number];

type BaseSoundData = {
  latin: string,
  path: PointTypeData[]
};

type BaseSoundMap = {
  [latin: string]: CharPath
};

type PartData = {
  id: string,
  positions: number[][]
};

type PartMap = {
  [id: string]: CharPartition[]
};

type SyllableData = {
  latin: string,
  partType: string
};

export type SavedFormat = {
  baseSounds: BaseSoundData[],
  parts: PartData[],
  syllables: SyllableData[]
};

function getBaseSounds(data: BaseSoundData[]): BaseSoundMap {
  const ret = {};

  data.forEach(({ latin, path }) => {
    ret[latin] = new CharPath(path.map(([...args]) => new Point(...args)));
  });

  return ret;
}

function getParts(data: PartData[]): PartMap {
  const ret = {};

  data.forEach(({ id, positions }) => {
    ret[id] = positions.map(xs => new CharPartition(...xs));
  });

  return ret;
}

export default class Syllabary {
  syllables: Syllable[];

  static fromJson(data: SavedFormat): Syllabary {
    const ret = new Syllabary();

    const parts = getParts(data.parts);
    const baseSounds = getBaseSounds(data.baseSounds);

    ret.syllables = data.syllables.map(({ latin, partType }) => {
      const partSet = parts[partType];

      const syllableParts = latin.split("").map((x, i) => {
        return new SyllablePart(partSet[i], baseSounds[x]);
      });

      return new Syllable(latin, syllableParts);
    });

    return ret;
  }
}
