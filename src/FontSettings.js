// @flow strict

export default class FontSettings {
  charSize: number = 21;

  /**
   * The size (as a ratio) of a character, which should be blank (on one side).
   *
   * So if it's 0.1, that is added to both sides, so there's 80 left for the
   * visible character shape.
   */
  charPadding: number = 0.05;

  strokeWidth: number = 0.05;

  visiblePadding: number;

  visibleWidth: number;

  visibleStroke: number;

  // $FlowFixMe
  constructor(opts = {}) {
    Object.keys(opts).forEach(key => {
      // $FlowFixMe
      if (this[key] === undefined) {
        throw new Error("You're setting a non-value.");
      }
      // $FlowFixMe
      this[key] = opts[key];
    });
    this.visiblePadding = this.charSize * this.charPadding;
    this.visibleWidth = this.charSize - 2 * this.visiblePadding;
    this.visibleStroke = this.strokeWidth * this.charSize;
  }
}
