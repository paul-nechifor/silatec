// @flow strict

import Point from "./Point";

export default class CharPath {
  points: Point[];

  constructor(points: Point[]) {
    this.points = points;
  }
}
