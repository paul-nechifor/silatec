// @flow strict

import React from "react";
import FontSettings from "./FontSettings";
import Syllable from "./Syllable";
import SyllableRepr from "./SyllableRepr";

export default function SyllablePair({
  syllable,
  settings
}: {
  syllable: Syllable,
  settings: FontSettings
}) {
  return (
    <div style={{ display: "inline-block", padding: 2 }}>
      <SyllableRepr syllable={syllable} settings={settings} />
      <div
        style={{
          textAlign: "center",
          color: "#888",
          fontSize: 10
        }}
      >
        {/* syllable.latin */}
      </div>
    </div>
  );
}
