// @flow strict

import CharPartition from "./CharPartition";
import FontSettings from "./FontSettings";

export type MoveTo = "M";

export type LineTo = "L";

export type PointMovementType = MoveTo | LineTo;

export default class Point {
  type: PointMovementType;

  points: number[];

  constructor(type: PointMovementType, ...points: number[]) {
    this.type = type;
    this.points = points;
  }

  getSvgPath(s: FontSettings, partition: CharPartition): string {
    const f = x => x * s.visibleWidth + s.visiblePadding;
    const fx = n => f(n * partition.width + partition.x);
    const fy = n => f(n * partition.height + partition.y);
    const ps = this.points;
    const mapPair = (_, i) => `${fx(ps[i * 2])},${fy(ps[i * 2 + 1])}`;
    const rangeLengthDiv2 = new Array(this.points.length / 2).fill();
    const points = rangeLengthDiv2.map(mapPair).join(" ");
    return `${this.type} ${points}`;
  }
}
