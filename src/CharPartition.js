// @flow strict

export default class CharPartition {
  x: number;

  y: number;

  width: number;

  height: number;

  constructor(
    x: number = 0,
    y: number = 0,
    width: number = 1,
    height: number = 1
  ) {
    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;
  }
}
