// @flow strict

import CharPartition from "./CharPartition";
import CharPath from "./CharPath";
import FontSettings from "./FontSettings";

export default class SyllablePart {
  partition: CharPartition;

  path: CharPath;

  constructor(partition: CharPartition, path: CharPath) {
    this.partition = partition;
    this.path = path;
  }

  getSvgPath(s: FontSettings): string {
    return this.path.points.map(p => p.getSvgPath(s, this.partition)).join(" ");
  }
}
