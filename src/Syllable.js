// @flow strict

import FontSettings from "./FontSettings";
import SyllablePart from "./SyllablePart";

export default class Syllable {
  latin: string;

  parts: SyllablePart[];

  constructor(latin: string, parts: SyllablePart[]) {
    this.latin = latin;
    this.parts = parts;
  }

  getSvgPath(s: FontSettings): string {
    return this.parts.map(x => x.getSvgPath(s)).join(" ");
  }
}
