// @flow

import React, { Component } from "react";
import { withStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import FontSettings from "./FontSettings";
import Syllabary from "./Syllabary";
import SyllablePair from "./SyllablePair";
import syllabaries from "./syllabaries";
import type { SavedFormat } from "./Syllabary";
import withRoot from "./withRoot";
import Menu from "./Menu";

type AppState = {
  syllabary: SavedFormat,
  syllabaryInput: string
};

class App extends Component<{}, AppState> {
  state = {
    syllabary: syllabaries.romsibtex,
    syllabaryInput: JSON.stringify(syllabaries.romsibtex, null, 2)
  };

  dataTextArea: { current: null | HTMLTextAreaElement };

  handleReload = (event: SyntheticEvent<HTMLButtonElement>) => {
    event.preventDefault();
    try {
      this.setState({ syllabary: JSON.parse(this.state.syllabaryInput) });
    } catch (SyntaxError) {
      alert("Invalid JSON."); // eslint-disable-line no-alert
    }
  };

  handleTextareaChange = (event: SyntheticInputEvent<HTMLTextAreaElement>) => {
    this.setState({ syllabaryInput: event.target.value });
  };

  constructor(props: void) {
    super(props);
    this.dataTextArea = React.createRef();
  }

  render() {
    const settings = new FontSettings({});
    const syllabary = Syllabary.fromJson(this.state.syllabary);

    return (
      <div className="App">
        <Menu />
        <textarea
          value={this.state.syllabaryInput}
          ref={this.dataTextArea}
          onChange={this.handleTextareaChange}
          style={{ width: 600, height: 200 }}
        />
        <br />

        <Button variant="contained" onClick={this.handleReload}>
          Reload now
        </Button>

        <div style={{ padding: 15 }} />

        <div style={{ width: 600, margin: "0 auto" }}>
          {syllabary.syllables.map((x, i) => (
            <SyllablePair key={i} syllable={x} settings={settings} />
          ))}
        </div>
      </div>
    );
  }
}

const styles = theme => ({
  root: {
    textAlign: "center",
    paddingTop: theme.spacing.unit * 20
  }
});

export default withRoot(withStyles(styles)(App));
