// @flow strict

const h = 0.5;
const p = 0.0;

export default {
  baseSounds: [
    {
      latin: "i",
      path: [["M", 1, 0], ["L", 1, 1], ["M", 0, 0.5], ["L", 1, 0.5]]
    },
    {
      latin: "î",
      path: [["M", 0.5, 0], ["L", 0.5, 1], ["M", 0, 0.5], ["L", 1, 0.5]]
    },
    {
      latin: "u",
      path: [["M", 0, 0], ["L", 0, 1], ["M", 0, 0.5], ["L", 1, 0.5]]
    },
    {
      latin: "e",
      path: [["M", 1, 0], ["L", 1, 1], ["M", 1, 0.5], ["L", 0, 1]]
    },
    {
      latin: "ă",
      path: [
        ["M", 0.5, 0],
        ["L", 0.5, 1],
        ["M", 0.5, 0.5],
        ["L", 0, 1],
        ["M", 0.5, 0.5],
        ["L", 1, 1]
      ]
    },
    {
      latin: "o",
      path: [["M", 0, 0], ["L", 0, 1], ["M", 0, 0.5], ["L", 1, 1]]
    },
    { latin: "a", path: [["M", 0.5, 0], ["L", 0.5, 1]] },
    { latin: "r", path: [["M", 0, 1], ["L", 1, 1], ["L", 1, 0]] },
    { latin: "l", path: [["M", 0, 0], ["L", 0, 1], ["L", 1, 1]] },
    { latin: "t", path: [["M", 0, 0], ["L", 1, 0], ["L", 1, 1]] },
    { latin: "d", path: [["M", 0, 1], ["L", 0, 0], ["L", 1, 0]] },
    { latin: "m", path: [["M", 0, 0], ["L", 1, 0], ["L", 1, 1], ["L", 0, 1]] },
    { latin: "n", path: [["M", 1, 1], ["L", 0, 1], ["L", 0, 0], ["L", 1, 0]] },
    { latin: "c", path: [["M", 0, 1], ["L", 0, 0], ["L", 1, 0], ["L", 1, 1]] },
    { latin: "g", path: [["M", 0, 0], ["L", 0, 1], ["L", 1, 1], ["L", 1, 0]] },
    { latin: "s", path: [["M", 1, 0], ["L", 0, 0], ["L", 1, 1], ["L", 0, 1]] },
    { latin: "z", path: [["M", 0, 0], ["L", 1, 0], ["L", 0, 1], ["L", 1, 1]] },
    {
      latin: "p",
      path: [
        ["M", 0, 1],
        ["L", 0, 0],
        ["L", 1, 0],
        ["L", 1, 1],
        ["M", 0, 0.5],
        ["L", 1, 0.5]
      ]
    },
    {
      latin: "b",
      path: [
        ["M", 0, 0],
        ["L", 0, 1],
        ["L", 1, 1],
        ["L", 1, 0],
        ["M", 0, 0.5],
        ["L", 1, 0.5]
      ]
    },
    {
      latin: "f",
      path: [
        ["M", 0, 1],
        ["L", 0, 0],
        ["L", 1, 0],
        ["M", 0, 0.5],
        ["L", 1, 0.5]
      ]
    },
    {
      latin: "v",
      path: [
        ["M", 0, 0],
        ["L", 1, 0],
        ["L", 1, 1],
        ["M", 0, 0.5],
        ["L", 1, 0.5]
      ]
    },
    {
      latin: "ș",
      path: [
        ["M", 0, 1],
        ["L", 1, 1],
        ["L", 1, 0],
        ["M", 0, 0.5],
        ["L", 1, 0.5]
      ]
    },
    {
      latin: "j",
      path: [
        ["M", 0, 0],
        ["L", 0, 1],
        ["L", 1, 1],
        ["M", 0, 0.5],
        ["L", 1, 0.5]
      ]
    }
  ],

  parts: [
    {
      id: "p1",
      positions: [[0, 0, 1, 1]]
    },
    {
      id: "p2v",
      positions: [[0, 0, 1, h - p], [0, h + p, 1, h - p]]
    },
    {
      id: "p2h",
      positions: [[0, 0, h - p, 1], [h + p, 0, h - p, 1]]
    },
    {
      id: "p3b",
      positions: [
        [0, 0, h - p, h - p],
        [0, h + p, h - p, h - p],
        [h + p, 0, h - p, 1]
      ]
    },
    {
      id: "p4",
      positions: [
        [0, 0, h - p, h - p],
        [0, h + p, h - p, h - p],
        [h + p, 0, h - p, h - p],
        [h + p, h + p, h - p, h - p]
      ]
    }
  ],

  syllables: [
    { latin: "i", partType: "p1" },
    { latin: "î", partType: "p1" },
    { latin: "u", partType: "p1" },
    { latin: "e", partType: "p1" },
    { latin: "ă", partType: "p1" },
    { latin: "o", partType: "p1" },
    { latin: "a", partType: "p1" },
    { latin: "r", partType: "p1" },
    { latin: "l", partType: "p1" },
    { latin: "t", partType: "p1" },
    { latin: "d", partType: "p1" },
    { latin: "m", partType: "p1" },
    { latin: "n", partType: "p1" },
    { latin: "c", partType: "p1" },
    { latin: "g", partType: "p1" },
    { latin: "s", partType: "p1" },
    { latin: "t", partType: "p1" },
    { latin: "p", partType: "p1" },
    { latin: "b", partType: "p1" },
    { latin: "f", partType: "p1" },
    { latin: "v", partType: "p1" },
    { latin: "ș", partType: "p1" },
    { latin: "j", partType: "p1" },
    { latin: "la", partType: "p2v" },
    { latin: "la", partType: "p2h" },
    { latin: "nir", partType: "p3b" },
    { latin: "tal", partType: "p3b" },
    { latin: "tan", partType: "p3b" },
    { latin: "pa", partType: "p2v" },
    { latin: "ul", partType: "p2h" },
    { latin: "es", partType: "p2h" },
    { latin: "te", partType: "p2v" },
    { latin: "nu", partType: "p2v" },
    { latin: "me", partType: "p2v" },
    { latin: "le", partType: "p2v" },
    { latin: "meu", partType: "p3b" },
    { latin: "cer", partType: "p3b" },
    { latin: "gu", partType: "p2v" },
    { latin: "ver", partType: "p3b" },
    { latin: "nu", partType: "p2v" },
    { latin: "lui", partType: "p3b" },
    { latin: "ro", partType: "p2v" },
    { latin: "mî", partType: "p2v" },
    { latin: "ni", partType: "p2v" },
    { latin: "ei", partType: "p2h" },
    { latin: "să", partType: "p2v" },
    { latin: "ne", partType: "p2v" },
    { latin: "spu", partType: "p3b" },
    { latin: "nă", partType: "p2v" },
    { latin: "pu", partType: "p2v" },
    { latin: "blic", partType: "p4" },
    { latin: "da", partType: "p2v" },
    { latin: "că", partType: "p2v" },
    { latin: "a", partType: "p1" },
    { latin: "su", partType: "p2v" },
    { latin: "pus", partType: "p3b" },
    { latin: "la", partType: "p2v" },
    { latin: "vot", partType: "p3b" },
    { latin: "nu", partType: "p2v" },
    { latin: "mi", partType: "p2v" },
    { latin: "rea", partType: "p3b" },
    { latin: "pro", partType: "p3b" },
    { latin: "cu", partType: "p2v" },
    { latin: "ro", partType: "p2v" },
    { latin: "ru", partType: "p2v" },
    { latin: "lui", partType: "p3b" },
    { latin: "e", partType: "p1" },
    { latin: "u", partType: "p1" },
    { latin: "ro", partType: "p2v" },
    { latin: "pean", partType: "p4" }
  ]
};
