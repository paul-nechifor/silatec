# Silatec

## Usage

Run tests:

    yarn test

See the coverage:

    yarn test:coverage

Run lint:

    yarn lint

Auto fix linting issues:

    yarn lint:fix

## License

ISC
